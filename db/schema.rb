# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_07_122054) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cep"
    t.string "street"
    t.string "number"
    t.string "district"
    t.string "complement"
    t.bigint "unit_id"
    t.index ["country_id"], name: "index_addresses_on_country_id"
    t.index ["unit_id"], name: "index_addresses_on_unit_id"
  end

  create_table "attendances", force: :cascade do |t|
    t.date "when"
    t.bigint "matter_school_year_id"
    t.bigint "student_id"
    t.integer "was"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["matter_school_year_id"], name: "index_attendances_on_matter_school_year_id"
    t.index ["student_id"], name: "index_attendances_on_student_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.bigint "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_cities_on_state_id"
  end

  create_table "classrooms", force: :cascade do |t|
    t.string "name"
    t.integer "active"
    t.string "stage"
    t.string "degree"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_emails", force: :cascade do |t|
    t.string "email"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "unit_id"
    t.index ["customer_id"], name: "index_contact_emails_on_customer_id"
    t.index ["unit_id"], name: "index_contact_emails_on_unit_id"
  end

  create_table "contact_phones", force: :cascade do |t|
    t.string "phone"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "unit_id"
    t.index ["customer_id"], name: "index_contact_phones_on_customer_id"
    t.index ["unit_id"], name: "index_contact_phones_on_unit_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_users", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_customer_users_on_customer_id"
    t.index ["user_id"], name: "index_customer_users_on_user_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.string "rg"
    t.integer "active"
    t.bigint "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_customers_on_address_id"
  end

  create_table "districts", force: :cascade do |t|
    t.string "name"
    t.string "street"
    t.string "number"
    t.string "complement"
    t.string "cep"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_districts_on_city_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "cbo"
    t.integer "discount"
    t.date "start_date"
    t.date "birth_date"
    t.string "cnh"
    t.string "category"
    t.integer "gender"
    t.string "nationality"
    t.string "marital_status"
    t.string "naturalness"
    t.string "father"
    t.string "mother"
    t.integer "pis"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "filiation_students", force: :cascade do |t|
    t.bigint "filiation_id"
    t.bigint "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["filiation_id"], name: "index_filiation_students_on_filiation_id"
    t.index ["student_id"], name: "index_filiation_students_on_student_id"
  end

  create_table "filiations", force: :cascade do |t|
    t.string "name"
    t.integer "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grades", force: :cascade do |t|
    t.float "value"
    t.integer "unity"
    t.bigint "student_id"
    t.bigint "matter_school_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["matter_school_year_id"], name: "index_grades_on_matter_school_year_id"
    t.index ["student_id"], name: "index_grades_on_student_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.bigint "product_customer_id"
    t.float "parcel"
    t.string "status"
    t.integer "number"
    t.date "due"
    t.integer "generation"
    t.bigint "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_customer_id"], name: "index_invoices_on_product_customer_id"
    t.index ["school_id"], name: "index_invoices_on_school_id"
  end

  create_table "logins", force: :cascade do |t|
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["email"], name: "index_logins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_logins_on_reset_password_token", unique: true
  end

  create_table "matter_school_years", force: :cascade do |t|
    t.bigint "matter_id"
    t.bigint "school_year_id"
    t.bigint "classroom_id"
    t.integer "active"
    t.string "school_days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classroom_id"], name: "index_matter_school_years_on_classroom_id"
    t.index ["matter_id"], name: "index_matter_school_years_on_matter_id"
    t.index ["school_year_id"], name: "index_matter_school_years_on_school_year_id"
  end

  create_table "matters", force: :cascade do |t|
    t.string "name"
    t.integer "active"
    t.integer "base"
    t.string "workload"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer "product_id"
    t.integer "price"
    t.string "status"
    t.string "buyer_name"
    t.string "reference"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_data", force: :cascade do |t|
    t.string "name"
    t.string "agency"
    t.string "count"
    t.string "wallet"
    t.string "token"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.bigint "unit_id"
    t.bigint "type_payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type_payment_id"], name: "index_payments_on_type_payment_id"
    t.index ["unit_id"], name: "index_payments_on_unit_id"
  end

  create_table "permissions", force: :cascade do |t|
    t.string "name"
    t.string "descricao"
    t.integer "active"
    t.string "subname"
    t.string "module_name"
    t.bigint "type_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type_user_id"], name: "index_permissions_on_type_user_id"
  end

  create_table "product_customers", force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "customer_id"
    t.float "parcel"
    t.bigint "student_id"
    t.date "initial_date"
    t.integer "quantity"
    t.float "discount"
    t.string "payment_link"
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_product_customers_on_customer_id"
    t.index ["product_id"], name: "index_product_customers_on_product_id"
    t.index ["student_id"], name: "index_product_customers_on_student_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "title"
    t.decimal "price"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "variation"
  end

  create_table "school_histories", force: :cascade do |t|
    t.date "birthdate"
    t.string "naturalness"
    t.string "nationality"
    t.string "document"
    t.string "state"
    t.string "birth_certificate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "school_years", force: :cascade do |t|
    t.string "name"
    t.date "date_start"
    t.date "date_end"
    t.date "year"
    t.integer "active"
    t.string "annual_average"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "social_name"
    t.string "cnpj"
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string "name"
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_states_on_country_id"
  end

  create_table "studant_matters", force: :cascade do |t|
    t.bigint "student_id"
    t.bigint "matter_id"
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["matter_id"], name: "index_studant_matters_on_matter_id"
    t.index ["student_id"], name: "index_studant_matters_on_student_id"
  end

  create_table "student_users", force: :cascade do |t|
    t.bigint "student_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_student_users_on_student_id"
    t.index ["user_id"], name: "index_student_users_on_user_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "registration"
    t.integer "active"
    t.bigint "customer_id"
    t.bigint "school_history_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_id"
    t.index ["customer_id"], name: "index_students_on_customer_id"
    t.index ["school_history_id"], name: "index_students_on_school_history_id"
    t.index ["school_id"], name: "index_students_on_school_id"
  end

  create_table "teacher_matters", force: :cascade do |t|
    t.bigint "teacher_id"
    t.bigint "matter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["matter_id"], name: "index_teacher_matters_on_matter_id"
    t.index ["teacher_id"], name: "index_teacher_matters_on_teacher_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "registration"
    t.integer "active"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_teachers_on_user_id"
  end

  create_table "type_payments", force: :cascade do |t|
    t.bigint "payment_data_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["payment_data_id"], name: "index_type_payments_on_payment_data_id"
  end

  create_table "type_permissions", force: :cascade do |t|
    t.bigint "permission_id"
    t.bigint "type_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["permission_id"], name: "index_type_permissions_on_permission_id"
    t.index ["type_user_id"], name: "index_type_permissions_on_type_user_id"
  end

  create_table "type_users", force: :cascade do |t|
    t.string "name"
    t.string "start_url"
    t.string "menu_start_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.integer "active"
    t.bigint "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_id"], name: "index_units_on_school_id"
  end

  create_table "user_permissions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "permission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["permission_id"], name: "index_user_permissions_on_permission_id"
    t.index ["user_id"], name: "index_user_permissions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.string "rg"
    t.integer "active"
    t.bigint "type_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "unit_id"
    t.bigint "login_id"
    t.index ["login_id"], name: "index_users_on_login_id"
    t.index ["type_user_id"], name: "index_users_on_type_user_id"
    t.index ["unit_id"], name: "index_users_on_unit_id"
  end

  add_foreign_key "addresses", "countries"
  add_foreign_key "addresses", "units"
  add_foreign_key "attendances", "matter_school_years"
  add_foreign_key "attendances", "students"
  add_foreign_key "cities", "states"
  add_foreign_key "contact_emails", "customers"
  add_foreign_key "contact_emails", "units"
  add_foreign_key "contact_phones", "customers"
  add_foreign_key "contact_phones", "units"
  add_foreign_key "customer_users", "customers"
  add_foreign_key "customer_users", "users"
  add_foreign_key "customers", "addresses"
  add_foreign_key "districts", "cities"
  add_foreign_key "filiation_students", "filiations"
  add_foreign_key "filiation_students", "students"
  add_foreign_key "grades", "matter_school_years"
  add_foreign_key "grades", "students"
  add_foreign_key "invoices", "product_customers"
  add_foreign_key "invoices", "schools"
  add_foreign_key "matter_school_years", "classrooms"
  add_foreign_key "matter_school_years", "matters"
  add_foreign_key "matter_school_years", "school_years"
  add_foreign_key "payments", "type_payments"
  add_foreign_key "payments", "units"
  add_foreign_key "permissions", "type_users"
  add_foreign_key "product_customers", "customers"
  add_foreign_key "product_customers", "products"
  add_foreign_key "product_customers", "students"
  add_foreign_key "states", "countries"
  add_foreign_key "studant_matters", "matters"
  add_foreign_key "studant_matters", "students"
  add_foreign_key "student_users", "students"
  add_foreign_key "student_users", "users"
  add_foreign_key "students", "customers"
  add_foreign_key "students", "school_histories"
  add_foreign_key "students", "schools"
  add_foreign_key "teacher_matters", "matters"
  add_foreign_key "teacher_matters", "teachers"
  add_foreign_key "teachers", "users"
  add_foreign_key "type_payments", "payment_data", column: "payment_data_id"
  add_foreign_key "type_permissions", "permissions"
  add_foreign_key "type_permissions", "type_users"
  add_foreign_key "units", "schools"
  add_foreign_key "user_permissions", "permissions"
  add_foreign_key "user_permissions", "users"
  add_foreign_key "users", "logins"
  add_foreign_key "users", "type_users"
  add_foreign_key "users", "units"
end
