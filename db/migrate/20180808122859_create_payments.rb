class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :unit, foreign_key: true
      t.references :type_payment, foreign_key: true

      t.timestamps
    end
  end
end
