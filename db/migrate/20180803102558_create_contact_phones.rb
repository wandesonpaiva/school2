class CreateContactPhones < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_phones do |t|
      t.string :phone
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
