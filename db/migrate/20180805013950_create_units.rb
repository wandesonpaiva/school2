class CreateUnits < ActiveRecord::Migration[5.1]
  def change
    create_table :units do |t|
      t.string :name
      t.integer :active
      t.references :school, foreign_key: true
      t.references :address, foreign_key: true
      t.references :contact_phone, foreign_key: true
      t.references :contact_email, foreign_key: true

      t.timestamps
    end
  end
end
