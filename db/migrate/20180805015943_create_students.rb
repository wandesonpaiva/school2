class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :registration
      t.integer :active
      t.references :unit, foreign_key: true
      t.references :customer, foreign_key: true
      t.references :school_history, foreign_key: true

      t.timestamps
    end
  end
end
