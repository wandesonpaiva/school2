class RemoveCityAndStateFromAddress < ActiveRecord::Migration[5.2]
  def change
    remove_column :addresses, :city, :string
    remove_column :addresses, :state, :string
  end
end
