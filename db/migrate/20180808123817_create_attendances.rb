class CreateAttendances < ActiveRecord::Migration[5.1]
  def change
    create_table :attendances do |t|
      t.date :when
      t.references :matter_school_year, foreign_key: true
      t.references :student, foreign_key: true
      t.integer :was

      t.timestamps
    end
  end
end
