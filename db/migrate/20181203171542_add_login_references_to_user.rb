class AddLoginReferencesToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :login, foreign_key: true
    remove_column :logins, :user_id
  end
end
