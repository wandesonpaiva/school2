class CreateStudantMatters < ActiveRecord::Migration[5.1]
  def change
    create_table :studant_matters do |t|
      t.references :student, foreign_key: true
      t.references :matter, foreign_key: true
      t.integer :active

      t.timestamps
    end
  end
end
