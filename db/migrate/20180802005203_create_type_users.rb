class CreateTypeUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :type_users do |t|
      t.string :name
      t.string :start_url
      t.string :menu_start_url

      t.timestamps
    end
  end
end
