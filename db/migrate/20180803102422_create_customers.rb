class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :cpf
      t.string :rg
      t.integer :active
      t.references :address, foreign_key: true

      t.timestamps
    end
  end
end
