class CreatePaymentData < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_data do |t|
      t.string :name
      t.string :agency
      t.string :count
      t.string :wallet
      t.string :token
      t.string :email

      t.timestamps
    end
  end
end
