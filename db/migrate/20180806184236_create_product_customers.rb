class CreateProductCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :product_customers do |t|
      t.references :product, foreign_key: true
      t.references :customer, foreign_key: true
      t.float :parcel
      t.references :student, foreign_key: true
      t.date :initial_date
      t.integer :quantity
      t.float :discount
      t.string :payment_link
      t.integer :active

      t.timestamps
    end
  end
end
