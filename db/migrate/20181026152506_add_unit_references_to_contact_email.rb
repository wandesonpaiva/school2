class AddUnitReferencesToContactEmail < ActiveRecord::Migration[5.2]
  def change
    add_reference :contact_emails, :unit, foreign_key: true
  end
end
