class AddVariationToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :variation, :int
  end
end
