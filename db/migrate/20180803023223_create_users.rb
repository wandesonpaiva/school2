class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :cpf
      t.string :rg
      t.integer :active
      t.references :type_user, foreign_key: true

      t.timestamps
    end
  end
end
