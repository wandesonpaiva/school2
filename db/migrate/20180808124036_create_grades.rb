class CreateGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :grades do |t|
      t.float :value
      t.integer :unity
      t.references :student, foreign_key: true
      t.references :matter_school_year, foreign_key: true

      t.timestamps
    end
  end
end
