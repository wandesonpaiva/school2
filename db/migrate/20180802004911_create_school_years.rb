class CreateSchoolYears < ActiveRecord::Migration[5.1]
  def change
    create_table :school_years do |t|
      t.string :name
      t.date :date_start
      t.date :date_end
      t.date :year
      t.integer :active
      t.string :annual_average

      t.timestamps
    end
  end
end
