class CreateDistricts < ActiveRecord::Migration[5.1]
  def change
    create_table :districts do |t|
      t.string :name
      t.string :street
      t.string :number
      t.string :complement
      t.string :cep
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
