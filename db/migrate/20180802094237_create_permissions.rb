class CreatePermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :permissions do |t|
      t.string :name
      t.string :descricao
      t.integer :active
      t.string :subname
      t.string :module_name
      t.references :type_user, foreign_key: true

      t.timestamps
    end
  end
end
