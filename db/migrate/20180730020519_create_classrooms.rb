class CreateClassrooms < ActiveRecord::Migration[5.1]
  def change
    create_table :classrooms do |t|
      t.string :name
      t.integer :active
      t.string :stage
      t.string :degree

      t.timestamps
    end
  end
end
