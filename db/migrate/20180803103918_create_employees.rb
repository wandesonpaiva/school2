class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :cbo
      t.integer :discount
      t.date :start_date
      t.date :birth_date
      t.string :cnh
      t.string :category
      t.integer :gender
      t.string :nationality
      t.string :marital_status
      t.string :naturalness
      t.string :father
      t.string :mother
      t.integer :pis

      t.timestamps
    end
  end
end
