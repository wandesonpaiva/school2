class CreateContactEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_emails do |t|
      t.string :email
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
