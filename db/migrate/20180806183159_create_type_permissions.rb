class CreateTypePermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :type_permissions do |t|
      t.references :permission, foreign_key: true
      t.references :type_user, foreign_key: true

      t.timestamps
    end
  end
end
