class CreateMatterSchoolYears < ActiveRecord::Migration[5.1]
  def change
    create_table :matter_school_years do |t|
      t.references :matter, foreign_key: true
      t.references :school_year, foreign_key: true
      t.references :classroom, foreign_key: true
      t.integer :active
      t.string :school_days

      t.timestamps
    end
  end
end
