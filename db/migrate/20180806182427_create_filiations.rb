class CreateFiliations < ActiveRecord::Migration[5.1]
  def change
    create_table :filiations do |t|
      t.string :name
      t.integer :type

      t.timestamps
    end
  end
end
