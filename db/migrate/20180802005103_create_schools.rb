class CreateSchools < ActiveRecord::Migration[5.1]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :social_name
      t.string :cnpj
      t.integer :active

      t.timestamps
    end
  end
end
