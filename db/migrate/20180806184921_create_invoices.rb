class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.references :product_customer, foreign_key: true
      t.float :parcel
      t.string :status
      t.integer :number
      t.date :due
      t.integer :generation
      t.references :school, foreign_key: true

      t.timestamps
    end
  end
end
