class CreateMatters < ActiveRecord::Migration[5.1]
  def change
    create_table :matters do |t|
      t.string :name
      t.integer :active
      t.integer :base
      t.string :workload

      t.timestamps
    end
  end
end
