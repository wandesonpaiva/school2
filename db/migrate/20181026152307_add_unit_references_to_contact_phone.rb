class AddUnitReferencesToContactPhone < ActiveRecord::Migration[5.2]
  def change
    add_reference :contact_phones, :unit, foreign_key: true
  end
end
