class CreateFiliationStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :filiation_students do |t|
      t.references :filiation, foreign_key: true
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
