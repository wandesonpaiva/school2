class AddFieldsToAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :cep, :string
    add_column :addresses, :street, :string
    add_column :addresses, :number, :string
    add_column :addresses, :district, :string
    add_column :addresses, :complement, :string
    add_column :addresses, :city, :string
    add_column :addresses, :state, :string
    add_reference :addresses, :unit, foreign_key: true
  end
end
