class RemoveContactPhoneReferencesFromUnit < ActiveRecord::Migration[5.2]
  def change
    remove_column :units, :contact_phone_id
    remove_column :units, :contact_email_id
    remove_column :units, :address_id
  end
end
