class AddSchoolReferencesToStudent < ActiveRecord::Migration[5.2]
  def change
    add_reference :students, :school, foreign_key: true
    remove_column :students, :unit_id
  end
end
