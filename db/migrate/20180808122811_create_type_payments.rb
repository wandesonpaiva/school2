class CreateTypePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :type_payments do |t|
      t.references :payment_data, foreign_key: true

      t.timestamps
    end
  end
end
