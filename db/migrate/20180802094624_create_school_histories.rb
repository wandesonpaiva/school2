class CreateSchoolHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :school_histories do |t|
      t.date :birthdate
      t.string :naturalness
      t.string :nationality
      t.string :document
      t.string :state
      t.string :birth_certificate

      t.timestamps
    end
  end
end
