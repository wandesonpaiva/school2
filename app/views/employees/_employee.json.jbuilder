json.extract! employee, :id, :cbo, :discount, :start_date, :birth_date, :cnh, :category, :gender, :nationality, :marital_status, :naturalness, :father, :mother, :pis, :created_at, :updated_at
json.url employee_url(employee, format: :json)
