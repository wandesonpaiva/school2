json.extract! school, :id, :name, :social_name, :cnpj, :active, :created_at, :updated_at
json.url school_url(school, format: :json)
