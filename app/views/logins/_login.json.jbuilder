json.extract! login, :id, :email, :active, :user_id, :created_at, :updated_at
json.url login_url(login, format: :json)
