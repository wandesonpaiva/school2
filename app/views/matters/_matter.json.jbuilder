json.extract! matter, :id, :name, :active, :base, :workload, :created_at, :updated_at
json.url matter_url(matter, format: :json)
