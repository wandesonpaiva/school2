json.extract! permission, :id, :name, :descricao, :active, :subname, :module_name, :type_user_id, :created_at, :updated_at
json.url permission_url(permission, format: :json)
