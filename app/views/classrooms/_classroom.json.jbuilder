json.extract! classroom, :id, :name, :active, :stage, :degree, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
