json.extract! customer, :id, :name, :cpf, :rg, :active, :address_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
