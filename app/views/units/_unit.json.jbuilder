json.extract! unit, :id, :name, :active, :school_id, :address_id, :contact_phone_id, :contact_email_id, :created_at, :updated_at
json.url unit_url(unit, format: :json)
