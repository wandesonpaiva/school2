json.extract! school_year, :id, :name, :date_start, :date_end, :year, :active, :annual_average, :created_at, :updated_at
json.url school_year_url(school_year, format: :json)
