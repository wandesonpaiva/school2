json.extract! product_custumer, :id, :product_id, :customer_id, :parcel, :student_id, :initial_date, :quantity, :discount, :payment_link, :active, :created_at, :updated_at
json.url product_custumer_url(product_custumer, format: :json)
