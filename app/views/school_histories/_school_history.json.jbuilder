json.extract! school_history, :id, :birthdate, :naturalness, :nationality, :document_id, :state, :birth_certificate, :created_at, :updated_at
json.url school_history_url(school_history, format: :json)
