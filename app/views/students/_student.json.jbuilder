json.extract! student, :id, :registration, :active, :unit_id, :customer_id, :school_history_id, :created_at, :updated_at
json.url student_url(student, format: :json)
