json.extract! payment_datum, :id, :name, :agency, :count, :wallet, :token, :email, :created_at, :updated_at
json.url payment_datum_url(payment_datum, format: :json)
