json.extract! invoice, :id, :product_custumer_id, :parcel, :status, :number, :due, :generation, :school_id, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
