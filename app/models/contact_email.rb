class ContactEmail < ApplicationRecord
  belongs_to :customer, required: false
  belongs_to :unit, required: false
end
