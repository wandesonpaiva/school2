class User < ApplicationRecord
  belongs_to :type_user, required: false
  belongs_to :unit, required: false
  belongs_to :login, required: false
  belongs_to :student, required: false
end
