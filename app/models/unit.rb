class Unit < ApplicationRecord
  belongs_to :school
  has_one :address
  has_one :contact_phone
  has_one :contact_email

  accepts_nested_attributes_for :address, :contact_phone

  has_many :users
end
