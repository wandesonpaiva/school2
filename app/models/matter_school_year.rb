class MatterSchoolYear < ApplicationRecord
  belongs_to :matter
  belongs_to :school_year
  belongs_to :classroom
end
