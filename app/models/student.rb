class Student < ApplicationRecord
  belongs_to :school, required: false
  belongs_to :customer, required: false
  belongs_to :school_history, required: false

  has_one :student_user
  has_one :user, :through => :student_user

  has_many :studant_matters
  has_many :grades
  has_many :attendances

  accepts_nested_attributes_for :student_user
  #accepts_nested_attributes_for :user

  #
  # Falta:
  # Product customer
  # Student User
  # Filliation Student
  #

end
