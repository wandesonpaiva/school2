class ContactPhone < ApplicationRecord
  belongs_to :unit, required: false
  belongs_to :customer, required: false
end
