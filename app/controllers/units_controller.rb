class UnitsController < ApplicationController
  before_action :set_unit, only: [:show, :edit, :update, :destroy]

  # GET /units
  # GET /units.json
  def index
    @page_header = "Unidades"
    @school = School.find(params[:school_id])
    @units = Unit.all
  end

  # GET /units/1
  # GET /units/1.json
  def show
    @page_header = "Detalhe Unidade"
  end

  # GET /units/new
  def new
    @page_header = "Nova Unidade"
    @unit = Unit.new
    @unit.build_address
    @unit.build_contact_phone
    @school = School.find(params[:school_id])
  end

  # GET /units/1/edit
  def edit
    @page_header = "Editar Unidade"
    @school = @unit.school
  end

  # POST /units
  # POST /units.json
  def create
    @school = School.find(params[:school_id])
    @unit = Unit.new(unit_params)
    @units = Unit.where(school_id: @school.id)

    respond_to do |format|
      if @unit.save
        @page_header = "Unidades"
        format.html { redirect_to({action: "index"}, notice: 'Unidade criada com sucesso.' )}
        format.json { render :show, status: :created, location: @unit }
      else
        @page_header = "Nova Unidade"
        format.html { render :new }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /units/1
  # PATCH/PUT /units/1.json
  def update
    @school = School.find(params[:school_id])
    @units = Unit.where(school_id: @school.id)

    respond_to do |format|
      if @unit.update(unit_params)
        #@unit.contact_phone.save
        format.html { redirect_to({action: "index"}, notice: 'Unidade foi atualizada com sucesso.' )}
        format.json { render :show, status: :ok, location: @unit }
      else
        format.html { render :edit }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /units/1
  # DELETE /units/1.json
  def destroy
    @unit.destroy
    respond_to do |format|
      format.html { redirect_to units_url, notice: 'Unidade foi apagada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unit
      @unit = Unit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def unit_params
      params.require(:unit).permit(:name, :active, :school_id, :contact_phone_id, :contact_email_id, address_attributes: [:cep, :street, :number, :district, :complement], contact_phone_attributes: [:phone])
    end
end
