class ProductCustumersController < ApplicationController
  before_action :set_product_custumer, only: [:show, :edit, :update, :destroy]

  # GET /product_custumers
  # GET /product_custumers.json
  def index
    @product_custumers = ProductCustumer.all
  end

  # GET /product_custumers/1
  # GET /product_custumers/1.json
  def show
  end

  # GET /product_custumers/new
  def new
    @product_custumer = ProductCustumer.new
  end

  # GET /product_custumers/1/edit
  def edit
  end

  # POST /product_custumers
  # POST /product_custumers.json
  def create
    @product_custumer = ProductCustumer.new(product_custumer_params)

    respond_to do |format|
      if @product_custumer.save
        format.html { redirect_to @product_custumer, notice: 'Product custumer was successfully created.' }
        format.json { render :show, status: :created, location: @product_custumer }
      else
        format.html { render :new }
        format.json { render json: @product_custumer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_custumers/1
  # PATCH/PUT /product_custumers/1.json
  def update
    respond_to do |format|
      if @product_custumer.update(product_custumer_params)
        format.html { redirect_to @product_custumer, notice: 'Product custumer was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_custumer }
      else
        format.html { render :edit }
        format.json { render json: @product_custumer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_custumers/1
  # DELETE /product_custumers/1.json
  def destroy
    @product_custumer.destroy
    respond_to do |format|
      format.html { redirect_to product_custumers_url, notice: 'Product custumer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_custumer
      @product_custumer = ProductCustumer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_custumer_params
      params.require(:product_custumer).permit(:product_id, :customer_id, :parcel, :student_id, :initial_date, :quantity, :discount, :payment_link, :active)
    end
end
