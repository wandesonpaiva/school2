Rails.application.routes.draw do

  resources (:schools) {resources :units}
  resources (:schools) {resources :students}
  
  devise_for :logins
  resources :payment_data
  resources :invoices
  resources :product_custumers
  #resources :students
  #resources :units
  resources :employees
  resources :customers
  resources :teachers
  resources :logins
  resources :users
  resources :school_histories
  resources :permissions
  resources :type_users
  resources :schools
  resources :school_years
  resources :matters
  resources :classrooms
  get 'notification/create'

  root to: 'home#index'
  
  get 'order/new'

  post 'order/create'

  get 'order/index'

  resources :products

  post 'notification', to: 'notification#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
