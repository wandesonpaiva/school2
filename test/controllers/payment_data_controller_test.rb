require 'test_helper'

class PaymentDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @payment_datum = payment_data(:one)
  end

  test "should get index" do
    get payment_data_url
    assert_response :success
  end

  test "should get new" do
    get new_payment_datum_url
    assert_response :success
  end

  test "should create payment_datum" do
    assert_difference('PaymentDatum.count') do
      post payment_data_url, params: { payment_datum: { agency: @payment_datum.agency, count: @payment_datum.count, email: @payment_datum.email, name: @payment_datum.name, token: @payment_datum.token, wallet: @payment_datum.wallet } }
    end

    assert_redirected_to payment_datum_url(PaymentDatum.last)
  end

  test "should show payment_datum" do
    get payment_datum_url(@payment_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_payment_datum_url(@payment_datum)
    assert_response :success
  end

  test "should update payment_datum" do
    patch payment_datum_url(@payment_datum), params: { payment_datum: { agency: @payment_datum.agency, count: @payment_datum.count, email: @payment_datum.email, name: @payment_datum.name, token: @payment_datum.token, wallet: @payment_datum.wallet } }
    assert_redirected_to payment_datum_url(@payment_datum)
  end

  test "should destroy payment_datum" do
    assert_difference('PaymentDatum.count', -1) do
      delete payment_datum_url(@payment_datum)
    end

    assert_redirected_to payment_data_url
  end
end
