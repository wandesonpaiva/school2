require 'test_helper'

class ProductCustumersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_custumer = product_custumers(:one)
  end

  test "should get index" do
    get product_custumers_url
    assert_response :success
  end

  test "should get new" do
    get new_product_custumer_url
    assert_response :success
  end

  test "should create product_custumer" do
    assert_difference('ProductCustumer.count') do
      post product_custumers_url, params: { product_custumer: { active: @product_custumer.active, customer_id: @product_custumer.customer_id, discount: @product_custumer.discount, initial_date: @product_custumer.initial_date, parcel: @product_custumer.parcel, payment_link: @product_custumer.payment_link, product_id: @product_custumer.product_id, quantity: @product_custumer.quantity, student_id: @product_custumer.student_id } }
    end

    assert_redirected_to product_custumer_url(ProductCustumer.last)
  end

  test "should show product_custumer" do
    get product_custumer_url(@product_custumer)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_custumer_url(@product_custumer)
    assert_response :success
  end

  test "should update product_custumer" do
    patch product_custumer_url(@product_custumer), params: { product_custumer: { active: @product_custumer.active, customer_id: @product_custumer.customer_id, discount: @product_custumer.discount, initial_date: @product_custumer.initial_date, parcel: @product_custumer.parcel, payment_link: @product_custumer.payment_link, product_id: @product_custumer.product_id, quantity: @product_custumer.quantity, student_id: @product_custumer.student_id } }
    assert_redirected_to product_custumer_url(@product_custumer)
  end

  test "should destroy product_custumer" do
    assert_difference('ProductCustumer.count', -1) do
      delete product_custumer_url(@product_custumer)
    end

    assert_redirected_to product_custumers_url
  end
end
