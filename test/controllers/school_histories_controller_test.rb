require 'test_helper'

class SchoolHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @school_history = school_histories(:one)
  end

  test "should get index" do
    get school_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_school_history_url
    assert_response :success
  end

  test "should create school_history" do
    assert_difference('SchoolHistory.count') do
      post school_histories_url, params: { school_history: { birth_certificate: @school_history.birth_certificate, birthdate: @school_history.birthdate, document_id: @school_history.document_id, nationality: @school_history.nationality, naturalness: @school_history.naturalness, state: @school_history.state } }
    end

    assert_redirected_to school_history_url(SchoolHistory.last)
  end

  test "should show school_history" do
    get school_history_url(@school_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_school_history_url(@school_history)
    assert_response :success
  end

  test "should update school_history" do
    patch school_history_url(@school_history), params: { school_history: { birth_certificate: @school_history.birth_certificate, birthdate: @school_history.birthdate, document_id: @school_history.document_id, nationality: @school_history.nationality, naturalness: @school_history.naturalness, state: @school_history.state } }
    assert_redirected_to school_history_url(@school_history)
  end

  test "should destroy school_history" do
    assert_difference('SchoolHistory.count', -1) do
      delete school_history_url(@school_history)
    end

    assert_redirected_to school_histories_url
  end
end
